# Serilog.Sinks.Slackbot

A Serilog sink that posts events to [Slack](https://slack.com/) as Slackbot.

**Package** - Serilog.Sinks.Slackbot
| **Platforms** - .NET 4.5

```csharp
var log = new LoggerConfiguration()
    .WriteTo.Slackbot(token: "token", team: "team", channel: "#general")
    .CreateLogger();
```
