﻿using System;
using System.Collections.Generic;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Formatting.Display;
using Serilog.Sinks.Slackbot;

namespace Serilog
{
    /// <summary>
    /// Adds the WriteTo.Slackbot() extension method to <see cref="LoggerConfiguration"/>.
    /// </summary>
    public static class LoggerConfigurationSlackbotExtensions
    {
        const string DefaultOutputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}";

        /// <summary>
        /// Adds a sink that posts log events to Slack.
        /// </summary>
        /// <param name="loggerConfiguration">The logger configuration.</param>
        /// <param name="token">Slackbot token.</param>
        /// <param name="team">Slack team name.</param>
        /// <param name="channel">Public Slack channel.</param>
        /// <param name="channelMentionLevels">@channel mention on select log event levels.</param>
        /// <param name="outputTemplate">A message template describing the format used to write to the sink.
        /// the default is "{Timestamp} [{Level}] {Message}{NewLine}{Exception}".</param>
        /// <param name="restrictedToMinimumLevel">The minimum log event level required in order to write an event to the sink.</param>
        /// <param name="batchPostingLimit">The maximum number of events to post in a single batch.</param>
        /// <param name="period">The time to wait between checking for event batches.</param>
        /// <param name="formatProvider">Supplies culture-specific formatting information, or null.</param>
        /// <returns>Logger configuration, allowing configuration to continue.</returns>
        /// <exception cref="ArgumentNullException">A required parameter is null.</exception>
        public static LoggerConfiguration Slackbot(
            this LoggerSinkConfiguration loggerConfiguration,
            string token,
            string team,
            string channel,
            List<LogEventLevel> channelMentionLevels = null,
            string outputTemplate = DefaultOutputTemplate,
            LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum,
            int batchPostingLimit = SlackbotSink.DefaultBatchPostingLimit,
            TimeSpan? period = null,
            IFormatProvider formatProvider = null)
        {
            if (loggerConfiguration == null) throw new ArgumentNullException("loggerConfiguration");
            if (token == null) throw new ArgumentNullException("token");
            if (team == null) throw new ArgumentNullException("team");
            if (channel == null) throw new ArgumentNullException("channel");

            var defaultedPeriod = period ?? SlackbotSink.DefaultPeriod;
            var formatter = new MessageTemplateTextFormatter(outputTemplate, formatProvider);

            return loggerConfiguration.Sink(
                new SlackbotSink(token, team, channel, channelMentionLevels, batchPostingLimit, defaultedPeriod, formatter),
                restrictedToMinimumLevel);
        }
    }
}
