﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Serilog.Events;
using Serilog.Sinks.PeriodicBatching;
using Serilog.Formatting;
using System.Threading.Tasks;

namespace Serilog.Sinks.Slackbot
{
    /// <summary>
    /// Posts log events to Slack.
    /// </summary>
    public class SlackbotSink : PeriodicBatchingSink
    {
        private readonly HttpClient _httpClient;
        private readonly ITextFormatter _textFormatter;
        private readonly List<LogEventLevel> _channelMentionLevels;
        private readonly string _token;
        private readonly string _channel;
        
        /// <summary>
        /// A reasonable default for the number of events posted in
        /// each batch.
        /// </summary>
        public const int DefaultBatchPostingLimit = 100;

        /// <summary>
        /// A reasonable default time to wait between checking for event batches.
        /// </summary>
        public static readonly TimeSpan DefaultPeriod = TimeSpan.FromSeconds(30);

        public SlackbotSink(
            string token, 
            string team, 
            string channel,
            List<LogEventLevel> channelMentionLevels, 
            int batchSizeLimit, 
            TimeSpan period, 
            ITextFormatter textFormatter) 
            : base(batchSizeLimit, period)
        {
            _token = token;
            _channel = channel;
            _channelMentionLevels = channelMentionLevels;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(string.Format("https://{0}.slack.com/services/hooks/", team))
            };

            _textFormatter = textFormatter;
        }

        protected override void EmitBatch(IEnumerable<LogEvent> events)
        {
            if (events == null)
                throw new ArgumentNullException("events");

            var payload = new StringWriter();

            if (_channelMentionLevels != null && events.Any(e => _channelMentionLevels.Contains(e.Level)))
                payload.Write("@channel: ");

            foreach (var logEvent in events)
            {
                _textFormatter.Format(logEvent, payload);
            }

            _httpClient.PostAsync(
                string.Format("slackbot?token={0}&channel={1}", _token, Uri.EscapeDataString(_channel)), 
                new StringContent(payload.ToString()));
        }
    }
}
